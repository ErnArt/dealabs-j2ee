import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import PostNewDeal from "../views/PostNewDeal.vue"
import RegisterPage from "../views/RegisterPage"
import SingleDeal from "../components/SingleDeal.vue"

Vue.use(VueRouter)

const routes = [
	{
		path: '/',
		name: 'Deals',
		component: Home
	},
	{
		path: "/deal/:id",
		name: "Deal",
		component: SingleDeal
	},
	{
		path: "/register",
		name: "Register",
		component: RegisterPage
	},
	{
		path: "/addNew",
		name: "AddNew",
		component: PostNewDeal
	},
	{
		path: '/about',
		name: 'About',
		// route level code-splitting
		// this generates a separate chunk (about.[hash].js) for this route
		// which is lazy-loaded when the route is visited.
		component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
	}
]

const router = new VueRouter({
	routes,
	mode: "history"
})

export default router
