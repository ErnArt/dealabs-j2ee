package univ.dlabs.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.dlabs.DAO.IUserRepository;
import univ.dlabs.DO.DealDO;
import univ.dlabs.DO.UserDO;
import univ.dlabs.DTO.SingleDealDTO;
import univ.dlabs.DTO.ShortUserDTO;
import univ.dlabs.DTO.UserDTO;

@Service
public class UserBO {

    @Autowired
    public IUserRepository userDAO;

    @Autowired
    private UserMapper userMapper;

    public UserDTO save(final UserDTO userDTO) {
        final UserDO userDO = userMapper.userDTOToUserDO(userDTO);
        userDAO.save(userDO);
        return userDTO;
    }
}
