package univ.dlabs.Services;

import org.springframework.stereotype.Service;
import univ.dlabs.DO.DealDO;
import univ.dlabs.DTO.DealDTO;
import univ.dlabs.DTO.SingleDealDTO;

@Service
public class DealMapper {

    public DealDTO dealDOToDealDTO(final DealDO dealDO) {
        final DealDTO dealDTO = new DealDTO();

        dealDTO.setId(dealDO.getId());
        dealDTO.setTitle(dealDO.getTitle());
        dealDTO.setCompany(dealDO.getCompany());
        dealDTO.setLink(dealDO.getLink());
        dealDTO.setPictureLink(dealDO.getPictureLink());
        dealDTO.setAuthor(dealDO.getCreator().getPseudo());
        dealDTO.setDate(dealDO.getDate());

        return dealDTO;
    }

    public SingleDealDTO dealDOToSingleDealDTO(final DealDO dealDO) {
        final SingleDealDTO singleDealDTO = new SingleDealDTO();

        singleDealDTO.setId(dealDO.getId());
        singleDealDTO.setTitle(dealDO.getTitle());
        singleDealDTO.setCompany(dealDO.getCompany());
        singleDealDTO.setLink(dealDO.getLink());
        singleDealDTO.setPriceOld(dealDO.getPriceOld());
        singleDealDTO.setPriceNew(dealDO.getPriceNew());
        singleDealDTO.setPromoCode(dealDO.getPromoCode());
        singleDealDTO.setPictureLink(dealDO.getPictureLink());
        singleDealDTO.setDescription(dealDO.getDescription());
        singleDealDTO.setAuthor(dealDO.getCreator().getPseudo());
        singleDealDTO.setDate(dealDO.getDate());

        return singleDealDTO;
    }

    public DealDO singleDealDTOToDealDO(final SingleDealDTO singleDealDTO) {
        final DealDO dealDO = new DealDO();

        dealDO.setId(singleDealDTO.getId());
        dealDO.setTitle(singleDealDTO.getTitle());
        dealDO.setCompany(singleDealDTO.getCompany());
        dealDO.setLink(singleDealDTO.getLink());
        dealDO.setPriceOld(singleDealDTO.getPriceOld());
        dealDO.setPriceNew(singleDealDTO.getPriceNew());
        dealDO.setPromoCode(singleDealDTO.getPromoCode());
        dealDO.setPictureLink(singleDealDTO.getPictureLink());
        dealDO.setDescription(singleDealDTO.getDescription());
        dealDO.setDate(singleDealDTO.getDate());

        return dealDO;
    }
}
