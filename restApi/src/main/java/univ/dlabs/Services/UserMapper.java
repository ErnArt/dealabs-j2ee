package univ.dlabs.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.dlabs.DO.UserDO;
import univ.dlabs.DTO.UserDTO;

@Service
public class UserMapper {

    @Autowired
    private PasswordBO passwordBO;

    public UserDO userDTOToUserDO(final UserDTO userDTO) {
        final UserDO userDO = new UserDO();
        userDO.setFirstName(userDTO.getFirstName());
        userDO.setLastName(userDTO.getLastName());
        userDO.setPseudo(userDTO.getPseudo());
        userDO.setPassword(passwordBO.encode(userDTO.getPassword()));
        return userDO;
    }
}
