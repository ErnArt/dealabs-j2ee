package univ.dlabs.DTO;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;
import java.util.Date;

public class SingleDealDTO {
    private Integer id;
    private String title;
    private String company;
    private String link;
    private Double priceOld;
    private Double priceNew;
    private String promoCode;
    private Integer degree;
    private String pictureLink;
    private String description;
    private String author;
    private Date date;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(final String company) {
        this.company = company;
    }

    public String getLink() {
        return link;
    }

    public void setLink(final String link) {
        this.link = link;
    }

    public Double getPriceOld() {
        return priceOld;
    }

    public void setPriceOld(final Double priceOld) {
        this.priceOld = priceOld;
    }

    public Double getPriceNew() {
        return priceNew;
    }

    public void setPriceNew(final Double priceNew) {
        this.priceNew = priceNew;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(final String promoCode) {
        this.promoCode = promoCode;
    }

    public Integer getDegree() {
        return degree;
    }

    public void setDegree(final Integer degree) {
        this.degree = degree;
    }

    public String getPictureLink() {
        return pictureLink;
    }

    public void setPictureLink(final String pictureLink) {
        this.pictureLink = pictureLink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(final String author) {
        this.author = author;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(final Date date) {
        this.date = date;
    }

}
