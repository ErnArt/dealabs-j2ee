package univ.dlabs.DTO;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * Extends du user spring (org.springframework.security.core.userdetails.User) si besoin d'ajout d'informations
 * @author Max
 *
 */
public class ShortUserDTO extends User {

    private static final long serialVersionUID = -2836522345185404025L;

    public ShortUserDTO(final String username, final String password, final Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

}
