package univ.dlabs.DTO;

import java.util.Date;

public class DealDTO {
    private Integer id;
    private String title;
    private String company;
    private String link;
    private Integer degree;
    private String pictureLink;
    private String author;
    private Date date;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(final String company) {
        this.company = company;
    }

    public String getLink() {
        return link;
    }

    public void setLink(final String link) {
        this.link = link;
    }

    public Integer getDegree() {
        return degree;
    }

    public void setDegree(final Integer degree) {
        this.degree = degree;
    }

    public String getPictureLink() {
        return pictureLink;
    }

    public void setPictureLink(final String pictureLink) {
        this.pictureLink = pictureLink;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(final String author) {
        this.author = author;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(final Date date) {
        this.date = date;
    }
}
