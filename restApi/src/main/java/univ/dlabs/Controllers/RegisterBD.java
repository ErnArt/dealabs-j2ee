package univ.dlabs.Controllers;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.web.bind.annotation.*;

import univ.dlabs.DO.UserDO;
import univ.dlabs.DTO.SingleDealDTO;
import univ.dlabs.DTO.UserDTO;
import univ.dlabs.Services.UserBO;

@RestController
@RequestMapping(value = "/public/bd/register")
@Transactional
public class RegisterBD {

    private static final Logger logger = LoggerFactory.getLogger(LoginBD.class);

    @Autowired
    private UserBO userBO;

    @CrossOrigin(origins = "*")
    @RequestMapping(method = RequestMethod.POST)
    public UserDTO save(@RequestBody final UserDTO userDTO) {
        return userBO.save(userDTO);
    }
}
