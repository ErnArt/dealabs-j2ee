package univ.dlabs.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import univ.dlabs.DO.TemperatureDO;

@Transactional(propagation = Propagation.MANDATORY)
public interface ITemperatureRepository extends JpaRepository<TemperatureDO, Integer> {
}
