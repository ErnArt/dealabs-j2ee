package univ.dlabs.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import univ.dlabs.DO.UserDO;

@Transactional(propagation = Propagation.MANDATORY)
public interface IUserRepository extends JpaRepository<UserDO, Integer> {
    @Query("from UserDO where pseudo=:pseudoParam")
    UserDO findUserWithName(@Param("pseudoParam") String pseudo);
}