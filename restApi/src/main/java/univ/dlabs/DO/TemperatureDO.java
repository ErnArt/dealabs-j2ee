package univ.dlabs.DO;

import org.apache.catalina.User;

import javax.persistence.*;

@Entity
@Table(name = "TBL_TEMPERATURE")
public class TemperatureDO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "VALUE")
    private Integer value;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "FK_DEAL")
    private DealDO deal;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "FK_USER")
    private UserDO user;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(final Integer value) {
        this.value = value;
    }

    public DealDO getDeal() {
        return deal;
    }

    public void setDeal(final DealDO deal) {
        this.deal = deal;
    }

    public UserDO getUser() {
        return user;
    }

    public void setUser(final UserDO user) {
        this.user = user;
    }
}